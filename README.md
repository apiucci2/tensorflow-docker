
This repository contains some basic dockerfiles and libraries to build a Docker image with Tensorflow with GPU support.
The images are based on Ubuntu 17.10 and 16.04, and also include Root installations and some other useful libraries.
In particular, the latest version built on Ubuntu 17.10 includes:
- GCC 7.2;
- Boost 1.59;
- ROOT 6.10.06;
- Nvidia drivers 384.90;
- Tensorflow 1.4.0, CUDA 8.0, cuDNN 7.0.

It seems that you have to install in the Docker image the exact NVidia driver version that you have on your host:
the dockerfiles now install the nvidia-384, but you may want to modify it accordignly to your host.
To verify the driver version that you have installed on your host, type:

```
nvidia-smi
```

#Build and run the Docker image

First get the cuDNN package that you need from this path:

```
/eos/lhcb/user/a/apiucci/Tensorflow_docker_packages
```

and place it in the same folder where the Docker file is.

Then build the latest available image:

```
source build_docker_Tensorflow.sh
```

and to run it:

```
source run_docker_Tensorflow.sh
```

Have fun!

# Setup the CI runner

Take a few coffees for this. But actually, this would be a bad idea if you care about your stress level.

## Setup an OpenStack instance
Following [these instructions](https://gist.github.com/sashabaranov/defccee8795619025d83f466b0ec4e35),
get for free an instance from the CERN OpenStack infrastructure and mount a CCC7 image.

## Setup a OpenStack volume attached to the instance
You may want to attach an external volume to the instance, to permanently save there the results of the CI run.
From the [CERN OpenStack-volume page](https://openstack.cern.ch/project/volumes/) create a volume, and attach it to the instance that you have just created.

Now you have to create a partition, from the volume you have just attached.
Login into your instance, and then create an ext4 partition, mounting it to a new `data` folder, and setting write permissions to the Universe:

```
sudo mkfs.ext4 /dev/vdb

sudo mkdir /data
sudo mount /dev/vdb /data

sudo chmod -R 777 /data
```

## Install docker on the instance
Install docker following using these commands:

```
yum update
yum install -y yum-utils

yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum makecache fast
yum install -y docker-ce

service docker start
docker ps
```

Some `sudo` could be needed somewhere.

Since Docker images will take quite some space on your instance,
let's define a new storage location of the images on the volume that we have attached above.
First find out where the Docker service configurations are loaded, searching in the output of this command:

Create a `/etc/docker/daemon.json` config file and fill it in this way:

```
{
    "graph": "/data/docker",
    "storage-driver": "overlay2"
}
```

Now reaload and restart the Docker service:

```
sudo systemctl daemon-reload
sudo systemctl start docker
```

and check that the Docker root directory is pointing to the `/data` folder:

```
docker info
```

You may have to try to kill and reload the daemon some times...

## Prepare the authorizations stuff
This is the painful part, but if you don't need to get any protected repository during your CI run, you can skip it.

Create an `ana-root` folder. Copy there the authorizations keys that you will need:

1. the Kerberos configurations of your CERN account, placed in the `/etc/krb5.conf` file of lxplus;
2. the private key that you will use for your CI runner. You can create a new key pair for this purpose, not using any passphrase to access it.
Name the private key as `id_cirunner`, and put it in the `ana-root` folder.
Add the public key as deployment key in all your submodule repositories.
If you are not able to add it as deploy key in some of the submodules (like because you don't have permissions), but you have access rights to the repository:
add the key to your Gitlab account as well. I know, this is a bad idea because from the point of view of the security, but I don't have any other workarounds now.
3. a Kerberos keytab for your account, to put in the `ana-root` folder. On lxplus creates it in this way:

```
> ktutil
ktutil:  addent -password -p username@CERN.CH -k 1 -e rc4-hmac
Password for username@CERN.CH: [enter your password]
ktutil:  addent -password -p username@CERN.CH -k 1 -e aes256-cts
Password for username@CERN.CH: [enter your password]
ktutil:  wkt username.keytab
ktutil:  quit
```

## Build the docker image and setup the gitlab-ci-runner
Copy this [Dockerfile](Dockerfile_CI) in the `ana-root` folder.
Remember to correctly set your GitLab user ID, email and the correct name of your Kerberos keytab in the Dockerfile.

Finally build the docker image where the analysis will run:

```
docker build -t ana-root ana-root/
```

## Build the docker image and setup the gitlab-ci-runner
Copy this [Docker file](Dockerfile) in the `ana-root` folder. Remember to correctly set your GitLab user ID,
email and the correct name of your Kerberos keytab in the Dockerfile.

Finally build the docker image where the analysis will run:

```
docker build -t ana-root ana-root/
```

Install the gitlab-runner in a docker image:

```
docker run -d --name gitlab-runner --restart always \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /etc/gitlab-runner:/etc/gitlab-runner \
  gitlab/gitlab-runner:v10.1.0
```

Use `latest` if you want, instead of a specific version.

Register the runner with your Gitlab repository:

```
docker exec -it gitlab-runner gitlab-runner register
```

complete the registration in this way:

```
[root@my-development-instance ~]# docker exec -it gitlab-runner gitlab-runner register
Running in system-mode.

Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
https://gitlab.cern.ch/ci

Please enter the gitlab-ci token for this runner:
[ID taken from https://gitlab.cern.ch/<username>/<project name>/runners]

Please enter the gitlab-ci description for this runner:
[8178263763e7]: my-awesome-runner

Please enter the gitlab-ci tags for this runner (comma separated):
[enter the runner name that you want]

Whether to run untagged builds [true/false]:
[false]: false

Whether to lock the Runner to current project [true/false]:
[true]: false

Registering runner... succeeded                     runner=gGxFcvut

Please enter the executor: parallels, shell, docker+machine, docker-ssh+machine, docker, docker-ssh, kubernetes, ssh, virtualbox:
docker

Please enter the default Docker image (e.g. ruby:2.1):
ana-root
```

Now edit the `/etc/gitlab-runner/config.toml` configuration file of you gitlab-ci-runner,
editing the `volumes` field with the mount point of your volume, like:

```
volumes = ["/cache","/data:/data:rw"]
```

and adding this line:

```
pull_policy = "if-not-present"
```

right after `volumes` for example.
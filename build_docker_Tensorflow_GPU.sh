
# login in the Docker hub
sudo docker login

# to build the Tensorflow image, you need to get the proper cuDNN package from the following path:
#     /eos/lhcb/user/a/apiucci/Tensorflow_docker_packages
# and put it in the same folder of the Docker file

# build the docker image
sudo docker build -t apiucci/tensorflow_gpu --tag tensorflow_2-3 . -f Dockerfile_TensorFlow_GPU_Ubuntu20-04

# if there is no Internet connection in the container:
# check your DNS:
#     nmcli dev show | grep 'IP4.DNS'
# update the Docker DNS:
#     sudo emacs -nw /etc/docker/daemon.json
# adding these with these lines (replacing the DNS of your system)
#     {
#        "dns": ["192.168.2.1", "8.8.8.8"]
#     }
# and finally a nice electro-shock therapy to the Docker service will fix everything:
#     sudo service docker restart


# run the docker image, using the NVidia drivers installed on the host
# in the run command you have to specify the nvidia devices installed on your host, that you can get with: ls -la /dev | grep nvidia
# for info: https://stackoverflow.com/questions/25185405/using-gpu-from-a-docker-container
docker run -ti --device /dev/nvidia0:/dev/nvidia0 --device /dev/nvidiactl:/dev/nvidiactl \
               --device /dev/nvidia-uvm:/dev/nvidia-uvm --device /dev/nvidia-modeset:/dev/nvidia-modeset apiucci/tensorflow-gpu /bin/bash 
